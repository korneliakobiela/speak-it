import React, { Component } from "react";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import SettingIcon from "@material-ui/icons/Settings";

import Add from "./components/add";
import Play from "./components/play";
import Show from "./components/show";
import Main from "./components/main";
import Settings from "./components/settings";

import "./App.css";
import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch
} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <AppBar position="sticky" color="primary" className="App-header">
              <Toolbar className="Toolbar-header">
                <Link to="/" className="nostyle">
                  <Typography variant="h6" color="inherit">
                    Speak it
                  </Typography>
                </Link>
                <Link to="/play" className="nostyle">
                  <Typography variant="h6" color="inherit">
                    Play
                  </Typography>
                </Link>
                <Link to="/show" className="nostyle">
                  <Typography variant="h6" color="inherit">
                    List
                  </Typography>
                </Link>
                <Link to="/settings" className="nostyle">
                  <SettingIcon />
                </Link>
              </Toolbar>
            </AppBar>

            <Switch>
              <Route path="/settings" component={Settings} />
              <Route path="/add" component={Add} />
              <Route path="/play" component={Play} />
              <Route path="/show" component={Show} />
              <Route path="/" component={Main} />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;

