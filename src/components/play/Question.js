import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import ItemCard from "./ItemCard.js";

export default class Question extends Component {
  render() {
    return (
      <div>
        <ItemCard 
           word={this.props.word}
           header="Please try remember the word"
        />
        <Button size="large" variant="contained" color="primary" onClick={this.props.finish}>
          FLIP
        </Button>
      </div>
    );
  }
}

