import React, { Component } from "react";

import Answer from "./Answer";
import Question from "./Question";
import Result from "./Result";
import PlayProgress from "./PlayProgress.js";
import "./style.css";
import Database from "./fasades/Database";
import Settings from "./fasades/Settings";

import { PAGES } from "./const.js";

const INIT_STATE = {
  pageShown: null,
  seenCount: 0,
  gameObject: null,
  dictionary: []
};

class Play extends Component {
  constructor() {
    super();
    this.state = INIT_STATE;
  }

  getUnseen() {
    return this.state.dictionary.filter(x => !x.isShown);
  }

  getRandomWord() {
    return Promise.resolve().then(() => {
      const unseen = this.getUnseen();
      if (unseen.length) {
        const index = Math.floor(Math.random() * unseen.length);
        return unseen[index].item;
      }
    });
  }

  componentWillMount() {
    this.startNewGame();
  }

  getAnswer() {
    this.setState({ ...this.state, pageShown: PAGES.ANSWER });
  }

  setWord(word) {
    this.setState({});
  }
  startNewRound() {
    Promise.resolve()
      .then(() => {
        return this.getRandomWord();
      })
      .then(gameObject => {
        return this.setState({
          pageShown: PAGES.QUESTION,
          gameObject: gameObject
        });
      });
  }

  initDictionary() {
    return Promise.resolve()
      .then(() => Settings.getDeckSize())
      .then(deckSize => Database.dbSelect(deckSize))
      .then(dictionary => {
        return this.setState({
          ...this.state,

          dictionary: dictionary.map(x => ({ item: x, isShown: false }))
        });
      });
  }

  startNewGame() {
    Promise.resolve()
      .then(() => this.setState(INIT_STATE))
      .then(() => this.initDictionary())
      .then(() => this.getRandomWord())
      .then(gameObject =>
        this.setState({
          ...this.state,
          seenCount: 0,
          pageShown: PAGES.QUESTION,
          gameObject: gameObject
        })
      );
  }

  registerSeenWord(result) {
    this.setState({
      ...this.state,
      seenCount: this.state.seenCount + 1,
      dictionary: this.state.dictionary.map(x =>
        x.item === this.state.gameObject
          ? { item: x, isShown: true, isGuessed: result }
          : x
      )
    });
  }

  finishRound(result) {
    this.registerSeenWord(result);
    if (this.getUnseen().length) this.startNewRound();
  }

  render() {
    if (!this.state.pageShown) return null;
    if (!Array.isArray(this.state.dictionary)) return null;

    if (this.state.dictionary.filter(x => !x.isShown).length === 0)
      return (
        <Result
          total={this.state.dictionary.length}
          correct={this.state.dictionary.filter(x => x.isGuessed).length}
          repeater={() => this.startNewGame()}
        />
      );
    return (
      <div className="play_container">
        <div>
          {this.state.pageShown === PAGES.ANSWER ? (
            <Answer
              word={this.state.gameObject.translated.word}
              description={this.state.gameObject.translated.description}
              finish={r => this.finishRound(r)}
            />
          ) : (
            <Question
              word={this.state.gameObject.origin.word}
              finish={() => this.getAnswer()}
            />
          )}

          <div className="progress_container">
            <PlayProgress
              value={
                (100 / this.state.dictionary.length) * this.state.seenCount
              }
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Play;

