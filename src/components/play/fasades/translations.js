export const dictionary = [
  {
    origin: {
      word: "elk"
    },
    translated: {
      description: "biggest of animals",
      word: "łoś"
    }
  },
  {
    origin: {
      word: "wolf"
    },
    translated: {
      description: "the most dangerous of animals",
      word: "wilk"
    }
  },
  {
    origin: {
      word: "hare"
    },
    translated: {
      description: "the most shy of animals",
      word: "zając"
    }
  },
  {
    origin: {
      word: "deer"
    },
    translated: {
      description: "the most noble of animals",
      word: "jeleń"
    }
  },
  {
    origin: {
      word: "river"
    },
    translated: {
      word: "rzeka",
      description: "The water that flows"
    }
  },
  {
    origin: {
      word: "forest"
    },
    translated: {
      word: "las",
      description: "A lot of trees"
    }
  },
  {
    origin: {
      word: "moon"
    },
    translated: {
      word: "księżyc",
      description: "The biggest at the heaven nighttimes"
    }
  },
  {
    origin: {
      word: "sun"
    },
    translated: {
      word: "słońce",
      description: "It produces daylight"
    }
  },
  {
    origin: {
      word: "star"
    },
    translated: {
      word: "gwiazda",
      description: "Nighttimes at the heaven"
    }
  },
  {
    origin: {
      word: "water"
    },
    translated: {
      word: "woda",
      description: "It also sterm, ice, and snow"
    }
  },
  {
    origin: {
      word: "soil"
    },
    translated: {
      word: "gleba",
      description: "Under your feet"
    }
  },
  {
    origin: {
      word: "tree"
    },
    translated: {
      word: "drzewo",
      description: "Very tall plant"
    }
  },
  {
    origin: {
      word: "bird"
    },
    translated: {
      word: "ptak",
      description: "Forever in feathers"
    }
  },
  {
    origin: {
      word: "hedgehog"
    },
    translated: {
      word: "jeż",
      description: "Forever in needles"
    }
  }
];

