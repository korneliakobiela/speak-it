import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Photo from "@material-ui/icons/AddAPhoto";
import Voice from "@material-ui/icons/RecordVoiceOver";
import SnapTaker from "./../snap";
import "./index.css";
import db from "../../db/db";
import FlashCard from "../../db/flashcard";
import "./upploader";
import ImageUtils from "../../utils/ImageUtil";
import AudioUtils from "../../utils/AudioUtil";
import TranslateUtils from "../../utils/TranslateUtil";


export default class Add extends Component {
  constructor(props) {
    super(props);
    this.state = {
      averse: "",
      reverse: "",
      isSnap: false,
      audio: "",
      recording: false,
      audioTracks: {}
    };
  }

  uploadToBucket(file) {
    const apiKey = "AIzaSyBykskwXGkczyYPuXtJewDy_rc_jFeFtNQ";
    const bucket = "the-image";
    return fetch(
      "https://www.googleapis.com/upload/storage/v1/b/" +
        bucket +
        "/o?key=" +
        apiKey +
        "&uploadType=media&name=land",
      {
        // Your POST endpoint
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          //   Authorization: "Bearer " + apiKey,
          "Content-Type": "image/png",
          "X-Upload-Content-Length": file.size,
          "X-Upload-Content-Type": "image/png"
        },
        body: file // This is your file object
      }
    )
      .then(
        response => response.json() // if the response is a JSON object
      )

      .catch(
        error => console.log(error) // Handle the error response object
      );
}
      uploadVoiceToBucket(file) {
        const apiKey = "AIzaSyBykskwXGkczyYPuXtJewDy_rc_jFeFtNQ";
        const bucket = "the-voice";
        return fetch(
          "https://www.googleapis.com/upload/storage/v1/b/" +
            bucket +
            "/o?key=" +
            apiKey +
            "&uploadType=media&name=land",
          {
            // Your POST endpoint
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              //   Authorization: "Bearer " + apiKey,
              "Content-Type": "audio/ogg",
              "X-Upload-Content-Length": file.size,
              "X-Upload-Content-Type": "audio/ogg"
            },
            body: file // This is your file object
          }
        )
          .then(
            response => response.json() // if the response is a JSON object
          )

          .catch(
            error => console.log(error) // Handle the error response object
          );
  }
  toggleSnap() {
    this.setState({ ...this.state, isSnap: !this.state.isSnap });
  }

  onPictureTaken(pictureBlob) {
    this.toggleSnap();
    //// PROCESS PICTURE

    console.log("picture blob");
    console.log(pictureBlob);

    this.uploadToBucket(pictureBlob)
      .then(response => {
        console.log(response);
        const link = response.mediaLink;
        return ImageUtils.getRecognition(link);
      })
      .then(recognition => {
        const description =
          recognition.responses[0].labelAnnotations[0].description;
        console.log("recognition");
        return description;
      })
      .then(desc => {
        this.setState({ ...this.state, averse: desc });
        return desc;
      })
      .then(desc => TranslateUtils.translate("en", "pl", desc))
      .then(translation => this.setState({ ...this.state, reverse: translation.data.translations[0].translatedText }));
  }

  avChange = e => {
    this.setState({ averse: e.target.value });
    TranslateUtils.translate('en', 'pl', e.target.value).then((translation) => {
      this.setState({...this.state, reverse: translation.data.translations[0].translatedText});
    })
  };

  reChange = e => {
    this.setState({ reverse: e.target.value });
  };

  onAdd = e => {
    const card = new FlashCard(this.state.averse, this.state.reverse);
    console.log(card);
    db.add(card).then(item => {
      this.setState({ averse: "", reverse: "" });
    });
  };

  onVoiceAdd = (e) => {
    if(!this.state.recording){
      const constraints = { audio: true, video: false };
      var chunks = [];

    navigator.mediaDevices.getUserMedia(constraints)
    .then((mediaStream) => {
      var mediaRecorder = new window.MediaRecorder(mediaStream);
      mediaRecorder.start();
      mediaRecorder.ondataavailable = function(e) {
          chunks.push(e.data);
        }
      this.setState({recording: true, audioTracks: mediaStream.getAudioTracks()});
      setTimeout(() => {
        const tracks = mediaStream.getAudioTracks();
        mediaRecorder.onstop = () => {
          var blob = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });
          chunks = [];
          var audioURL = window.URL.createObjectURL(blob);
          this.setState({audio: audioURL, averse: "hacking"});
          this.uploadVoiceToBucket(blob)
            .then(response => {
              console.log(response);
              const link = response.mediaLink;
              console.log(link)
              return AudioUtils.getRecognition(link);
            }).then((word) => {
              console.log(word);
              TranslateUtils.translate('en', 'pl', this.state.averse).then((translation) => {
                this.setState({...this.state, reverse: translation.data.translations[0].translatedText});
              })
            });

        }
        mediaRecorder.stop();
        tracks[0].stop();
        this.setState({recording:false});
      }, 5000);
    })}
  }

  onTranslate = () => {

  }

  render() {
    return (
      <div className="Add">
        <form className="Add-form">
          <div className="Add-form-buttons">
            <Button
              variant="outlined"
              color="primary"
              size="large"
              onClick={() => this.toggleSnap()}
            >
              <Photo />
            </Button>
            {this.state.isSnap ? (
              <SnapTaker
                onPictureTaken={pictureBlob => this.onPictureTaken(pictureBlob)}
              />
            ) : null}
            <Button variant="outlined" color={this.state.recording ? "secondary": "primary"} size="large" onClick={this.onVoiceAdd}>
              <Voice />
            </Button>
          </div>
          <audio src={this.state.audio} controls style={{width:"100%"}}/>
          <TextField
            label="Question"
            fullWidth
            className="Add-form-item"
            onChange={this.avChange}
            value={this.state.averse}
          />
          <TextField
            label="Answer"
            fullWidth
            className="Add-form-item"
            onChange={this.reChange}
            value={this.state.reverse}
          />
          <Button
            variant="contained"
            color="primary"
            size="large"
            className="Add-form-item"
            onClick={this.onAdd}
          >
            Add Question
          </Button>
        </form>
      </div>
    );
  }
}
