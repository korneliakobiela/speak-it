import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import Show from "../show";
import "./index.css";
import {
  Link
} from "react-router-dom";

class Main extends Component {
  render() {
    return (
      <div className="Main-screen">
        <div className="Main-content">
          <Show />
        </div>
        <Link to="/add">
          <Button
            variant="fab"
            color="primary"
            aria-label="Add"
            className="Add-button"
          >
            <AddIcon />
          </Button>
        </Link>
      </div>
    );
  }
}
export default Main;

